package aula.fib.br.customizedlist

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView

class CustomizedList : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customized_list2)

        val listaContatos = ArrayList<Contato>()
        val c1 = Contato(1L, "Hatake Kakashi", "Kakashi Hatake (はたけカカシ, Hatake Kakashi) é um Shinobi de Konohagakure. Ele recebeu um Sharingan de seu ex-companheiro de equipe, Obito Uchiha, quando era mais jovem, fazendo-o ser conhecido como Kakashi o Ninja Copiador (コピー忍者のカカシ, Kopī Ninja no Kakashi) e Kakashi do Sharingan (写輪眼のカカシ, Sharingan no Kakashi). Seu prodigioso talento, habilidades e destreza com o Sharingan fizeram dele um dos mais capazes ninjas da aldeia, sendo reconhecido em todo o mundo ninja")
        val c2 = Contato(2L, "Uzumaki Naruto", "Naruto Uzumaki (うずまきナルト, Uzumaki Naruto) é um shinobi de Konohagakure, também sendo a reencarnação atual de Asura e o protagonista homônimo da franquia Naruto. Ele se tornou o jinchūriki de Kurama no dia de seu nascimento, um destino que o levou a ser condenado ao ostracismo e ser negligenciado por toda a aldeia durante toda a sua infância. Depois de entrar para o Time Kakashi, Naruto trabalhou duro para ganhar o respeito e o reconhecimento da aldeia, com o sonho de se tornar Hokage. ")
        val c3 = Contato(3L, "Uzumaki Nagato", "Nagato (長門, Nagato) foi um órfão de Amegakure e um aluno de Jiraiya, que cresceu durante a Segunda Guerra Mundial Shinobi. Órfão pelo conflito, Nagato se uniu juntamente com seus companheiros órfãos de guerra para formar a Akatsuki, uma organização cujos objetivos foram parar os ciclos infinitos de morte. Depois de seu amigo e líder, Yahiko, ser morto por pregar a paz, Nagato se convenceu de que o mundo nunca iria parar de lutar voluntariamente a menos que ele conheça o que é a verdadeira dor. Ele adotou o pseudônimo de Pain (ペイン, Pein) e foi o novo líder da Akatsuki na tentativa de mudar o mundo a um estado de paz através do medo da destruição pelas mãos da Akatsuki.")
        val c4 = Contato(4L, "Uchiha Madara", "Madara Uchiha (うちはマダラ, Uchiha Madara) foi um lendário líder do clã Uchiha. Ele fundou Konohagakure ao lado de seu rival, Hashirama Senju, com a intenção de iniciar uma era de paz. Quando os dois não conseguiram concordar em como conseguir a paz, eles lutaram pelo controle da aldeia, um conflito que terminou na morte de Madara. Ele reescreveu sua morte e se escondeu para trabalhar em seus planos para acabar com os conflitos mundiais. Incapaz de conclui-los em sua fase de vida natural, ele confiou o seu conhecimento e planos para Obito Uchiha, pouco antes de sua morte real. Anos mais tarde, Madara foi revivido, e viu seus planos serem frustrados antes de morrer uma última vez.")
        val c5 = Contato(5L, "Sasuke Uchiha", "asuke Uchiha (うちは サスケ, Uchiha Sasuke?) é um personagem fictício da série de anime e mangá Naruto criado por Masashi Kishimoto. Na história fictícia da série, Sasuke é membro do extinto clã Uchiha, que era uma habilidosa família de ninjas da Vila Oculta da Folha. Seu objetivo inicial é vingar a destruição de seu clã, matando seu irmão Itachi Uchiha, que assassinou quase todos sozinho. Inicialmente é frio e guiado por sua vingança, mas posteriormente cria vínculos de amizades com outros personagens, particularmente com Naruto Uzumaki, quem ele passa a considerar como rival.")
        val c6 = Contato(6L, "Ootsuki Kaguya", "A princesa Kaguya Ōtsutsuki (大筒木かぐや, Ōtsutsuki Kaguya) é a matriarca do clã Ōtsutsuki e mãe de Hagoromo Ōtsutsuki e Hamura Ōtsutsuki, que viveu muito antes da fundação das aldeias ocultas, durante uma era de guerras intermináveis entre a humanidade. Ela consumiu o fruto da Árvore Divina e se tornou a progenitora do chakra, além de fazer parte da criação da besta que se tornaria conhecida como Dez-Caudas.")

        listaContatos.add(c1)
        listaContatos.add(c2)
        listaContatos.add(c3)
        listaContatos.add(c4)
        listaContatos.add(c5)
        listaContatos.add(c6)

        val adapter = ContatoAdapter(applicationContext, listaContatos, assets)

        val lista = findViewById<ListView>(R.id.lista)
        lista.setAdapter(adapter)

        lista.setOnItemClickListener {parent, view, position, id ->
            val contato = listaContatos.get(position)
            val intent = Intent(this, DetalheContato::class.java)
            intent.putExtra("contato", contato)
            startActivity(intent)
        }
    }
}
