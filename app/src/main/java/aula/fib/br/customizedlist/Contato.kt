package aula.fib.br.customizedlist

import java.io.Serializable

data class Contato(var id: Long,
                   var shinobiname: String,
                   val descshinobi: String? = null) : Serializable